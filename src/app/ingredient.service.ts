import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Ingredient } from './ingredient';

@Injectable({
  providedIn: 'root'
})
export class IngredientService {

  private baseURL = "http://localhost:8080/api/v1/";
  constructor(private httpClient: HttpClient) { }

  getIngredientList(): Observable<Ingredient[]>{
    return this.httpClient.get<Ingredient[]>(`${this.baseURL+'ingredients'}`)
  }

  addIngredient(ingredient: Ingredient): Observable<Object>{
    var headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
    return this.httpClient.post(`${this.baseURL+'saveIngredient'}`, ingredient);
  }

  getIngredientById(id: string): Observable<Ingredient>{
    return this.httpClient.get<Ingredient>(`${this.baseURL+"ingredient?id="}${id}`);
  }

  updateIngredient(id: string, ingredient: Ingredient): Observable<Object>{
    return this.httpClient.put(`${this.baseURL+'updateIngredient'}`, ingredient);
  }

  deleteIngredient(id: string): Observable<Object>{
    return this.httpClient.delete(`${this.baseURL+"deleteIngredient?id="}${id}`);
  }
}

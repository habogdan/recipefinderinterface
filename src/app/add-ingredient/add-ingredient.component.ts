import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Ingredient } from '../ingredient';
import { IngredientService } from '../ingredient.service';

@Component({
  selector: 'app-add-ingredient',
  templateUrl: './add-ingredient.component.html',
  styleUrls: ['./add-ingredient.component.css']
})
export class AddIngredientComponent implements OnInit {

  ingredient: Ingredient = new Ingredient();

  constructor( private ingredientService: IngredientService, private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit(){
    console.log(this.ingredient);
    this.saveIngredient();
  }

  saveIngredient(){
    this.ingredientService.addIngredient(this.ingredient).subscribe(data =>{
      console.log(data);
      this.goToIngredientList();
    },
    error => console.log(error));
  }

  goToIngredientList(){
    this.router.navigate(['/ingredients']);
  }

}

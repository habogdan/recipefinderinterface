import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Ingredient } from '../ingredient';
import { IngredientService } from '../ingredient.service';

@Component({
  selector: 'app-ingredient-details',
  templateUrl: './ingredient-details.component.html',
  styleUrls: ['./ingredient-details.component.css']
})
export class IngredientDetailsComponent implements OnInit {

  id: string
  ingredient: Ingredient
  constructor(private route: ActivatedRoute,
    private ingredientService: IngredientService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    this.ingredient = new Ingredient();
    this.ingredientService.getIngredientById(this.id).subscribe( data => {
      this.ingredient = data;
    })
  }

}

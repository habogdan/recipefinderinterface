import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Ingredient } from '../ingredient';
import { IngredientService } from '../ingredient.service';

@Component({
  selector: 'app-ingredient-list',
  templateUrl: './ingredient-list.component.html',
  styleUrls: ['./ingredient-list.component.css']
})
export class IngredientListComponent implements OnInit {

  ingredients: Ingredient[];

  constructor(private ingredientService: IngredientService, private router: Router) { }

  ngOnInit(): void {
    this.getIngredients();
  }

  private getIngredients(){
    this.ingredientService.getIngredientList().subscribe(data => {
      console.log(data);
      this.ingredients = data;
    });
  }

  updateIngredient(id: string){
    this.router.navigate(['update-ingredient', id])

  }

  deleteIngredient(id: string){
    this.ingredientService.deleteIngredient(id).subscribe(data => {
      console.log(data);
      this.getIngredients();
    })
  }

  ingredientDetails(id: string){
    this.router.navigate(['ingredient-details', id])
  }

}

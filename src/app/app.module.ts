import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IngredientListComponent } from './ingredient-list/ingredient-list.component';
import { AddIngredientComponent } from './add-ingredient/add-ingredient.component';
import { UpdateIngredientComponent } from './update-ingredient/update-ingredient.component';
import { IngredientDetailsComponent } from './ingredient-details/ingredient-details.component';

@NgModule({
  declarations: [
    AppComponent,
    IngredientListComponent,
    AddIngredientComponent,
    UpdateIngredientComponent,
    IngredientDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

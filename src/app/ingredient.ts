export class Ingredient {
    id: string;
    name: string;
    photoURL: string;
}

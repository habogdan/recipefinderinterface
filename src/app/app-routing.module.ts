import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddIngredientComponent } from './add-ingredient/add-ingredient.component';
import { IngredientDetailsComponent } from './ingredient-details/ingredient-details.component';
import { IngredientListComponent } from './ingredient-list/ingredient-list.component';
import { UpdateIngredientComponent } from './update-ingredient/update-ingredient.component';

const routes: Routes = [
  {path: 'ingredients', component: IngredientListComponent},
  {path: 'add-ingredient', component: AddIngredientComponent},
  {path: '', redirectTo: 'ingredients', pathMatch: 'full'},
  {path: 'update-ingredient/:id', component: UpdateIngredientComponent},
  {path: 'ingredient-details/:id', component: IngredientDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
